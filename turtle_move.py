#!/usr/bin/env python
import rospy
import numpy as np
from turtlesim.msg import Pose
from geometry_msgs.msg import Twist

class TurtleMove(object):
   def __init__(self):
       rospy.init_node("turtle_move")

       self.vel_pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
       self.rate = rospy.Rate(20)

       self.turtle_sub = rospy.Subscriber("/turtle1/pose", Pose, self.turtle_move_callback)

       self.turtle_pose = Pose()
       self.vel_cmd = Twist()

   def turtle_move_callback(self, data):
       self.turtle_pose = data
       self.vel_cmd.linear.x = 2.0
       self.vel_cmd.angular.z = 1.8

   def start_turtleop(self):
       while not rospy.is_shutdown():
           self.vel_pub.publish(self.vel_cmd)
           self.rate.sleep()

if __name__ == "__main__":
   turtle_move = TurtleMove()
   turtle_move.start_turtleop()

